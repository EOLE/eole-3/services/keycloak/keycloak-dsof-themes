<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=social.displayInfo displayWide=(realm.password && social.providers??); section>
    <#if section = "header">
        ${msg("doLogIn")}
    <#elseif section = "form">
      <div id="kc-login" class="rf-grid-row rf-grid-row--gutters">
      <div id="kc-form-wrapper" class="rf-col">
      <#if realm.password && social.providers?? && social.providers?size gt 0>
          <div id="kc-social-providers"  class="rf-col block-socialprovider">
              <h3>Se connecter avec une identité nationale</h3>
              <ul class="rf-tag-list">
                  <#list social.providers as p>
                     <#if p.alias?? && p.alias == "agentconnect">
                        <li class="zocial-item zocial-item-${p.alias}" >
                          <a href="${p.loginUrl}" id="zocial-${p.alias}" class="rf-tag ${p.providerId}  <#if social.providers?size gt 3>${properties.kcFormSocialAccountGridItem!}</#if>"
                               type="button" title="${p.displayName}">
                          </a>
                        </li>
                      <#else>
                          <#if p.alias?? && p.alias == "hub-education">
                            <li class="zocial-item zocial-item-hubeducation" >
                              <a href="${p.loginUrl}" id="zocial-hubeducation" class="rf-tag hubeducation  <#if social.providers?size gt 3>${properties.kcFormSocialAccountGridItem!}</#if>"
                                      type="button" title="${p.displayName}">
                              </a>
                            </li>
                          <#else>
                              <#if p.alias?? && p.alias == "apps-education">
                                <li class="zocial-item zocial-item-appseducation" >
                                  <a href="${p.loginUrl}" id="zocial-appseducation" class="rf-tag appseducation  <#if social.providers?size gt 3>${properties.kcFormSocialAccountGridItem!}</#if>"
                                           type="button" title="${p.displayName}">
                                  </a>
                                </li>
                              <#else>
                                  <#if p.alias?? && p.alias == "sspcloud">
                                    <li class="zocial-item zocial-item-datalab" >
                                      <a href="${p.loginUrl}" id="zocial-datalab" class="rf-tag datalab  <#if social.providers?size gt 3>${properties.kcFormSocialAccountGridItem!}</#if>"
                                               type="button" title="${p.displayName}">
                                      </a>
                                    </li>
                                  <#else>
                                      <#if p.alias?? && p.alias == "mim-libre">
                                           <li class="zocial-item zocial-item-mimlibre" >
                                             <a href="${p.loginUrl}" id="zocial-mimlibre" class="rf-tag mimlibre  <#if social.providers?size gt 3>${properties.kcFormSocialAccountGridItem!}</#if>"
                                                     type="button" title="${p.displayName}">
                                             </a>
                                           </li>
                                      <#else>
                                          <#if p.alias?? && p.alias == "moncomptepro">
                                               <li class="zocial-item zocial-item-${p.alias}" >
                                                 <a href="${p.loginUrl}" id="zocial-${p.alias}" class="rf-tag ${p.alias} <#if social.providers?size gt 3>${properties.kcFormSocialAccountGridItem!}</#if>"
                                                         type="button" title="${p.displayName}">
                                                 </a>
                                               </li>
                                          <#else>
                                              <#if p.alias?? && p.alias == "eole3dev">
                                                   <li class="zocial-item zocial-item-eole3dev" >
                                                     <a href="${p.loginUrl}" id="zocial-eole3dev" class="rf-tag eole3dev  <#if social.providers?size gt 3>${properties.kcFormSocialAccountGridItem!}</#if>"
                                                             type="button" title="${p.displayName}">
                                                     </a>
                                                   </li>
                                              <#else>
                                                   <li class="zocial-item social-item-${p.alias}">
                                                        <a id="social-${p.alias}" class="${properties.kcFormSocialAccountListButtonClass!} <#if social.providers?size gt 3>${properties.kcFormSocialAccountGridItem!}</#if>"
                                                                type="button" href="${p.loginUrl}">
                                                           <#if p.iconClasses?has_content>
                                                                <i class="${properties.kcCommonLogoIdP!} ${p.iconClasses!}" aria-hidden="true"></i>
                                                                <span class="${properties.kcFormSocialAccountNameClass!} kc-social-icon-text">${p.displayName!}</span>
                                                           <#else>
                                                                <span class="${properties.kcFormSocialAccountNameClass!}">${p.displayName!}</span>
                                                           </#if>
                                                        </a>
                                                   </li>
                                              </#if>
                                          </#if>
                                      </#if>
                                  </#if>
                              </#if>
                          </#if>
                      </#if>
                  </#list>
              </ul>
          </div>
      </#if>
        <#if realm.password>
          <br/>
          <form id="kc-form-login" onsubmit="login.disabled = true; return true;" action="${url.loginAction}" method="post">
              <div class="rf-mb-3w rf-input-group">
                  <h3>Se connecter avec un compte fonctionnel ou local</h3>
                  <label for="username" class="rf-label"><#if !realm.loginWithEmailAllowed>${msg("username")}<#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmail")}<#else>${msg("email")}</#if></label>

                  <#if usernameEditDisabled??>
                      <input id="username" class="rf-input" name="username" value="${(login.username!'')}" type="text" disabled />
                  <#else>
                      <input id="username" class="rf-input" name="username" value="${(login.username!'')}"  type="text" autofocus autocomplete="off" />
                  </#if>
              </div>

              <div class="rf-mb-3w rf-input-group">
                  <label for="password" class="rf-label">${msg("password")}</label>
                  <input id="password" class="rf-input" name="password" type="password" autocomplete="off" />
              </div>

              <div>
                  <div id="kc-form-options">
                      <#if realm.rememberMe && !usernameEditDisabled??>
                          <div class="checkbox">
                              <label>
                                  <#if login.rememberMe??>
                                      <input id="rememberMe" name="rememberMe" type="checkbox" checked> ${msg("rememberMe")}
                                  <#else>
                                      <input id="rememberMe" name="rememberMe" type="checkbox"> ${msg("rememberMe")}
                                  </#if>
                              </label>
                          </div>
                      </#if>
                      </div>
                      <div class="${properties.kcFormOptionsWrapperClass!}">
                          <#if realm.resetPasswordAllowed>
                              <span><a href="${url.loginResetCredentialsUrl}">${msg("doForgotPassword")}</a></span>
                          </#if>
                      </div>

                </div>

                <div id="kc-form-buttons" class="rf-input-group">
                    <#if auth.selectedCredential?has_content>
                        <input type="hidden" id="id-hidden-input" name="credentialId" value="${auth.selectedCredential}" />
                    <#else>
                        <input type="hidden" id="id-hidden-input" name="credentialId"/>
                    </#if>
                    <button class="rf-btn rf-btn--primary" name="login" id="kc-login" type="submit">${msg("doLogIn")}</button>
                </div>
          </form>
        </#if>
      </div>
      </div>
    <#elseif section = "info" >
        <#if realm.password && realm.registrationAllowed && !registrationDisabled??>
            <div id="kc-registration">
                <span>${msg("noAccount")} <a tabindex="6" class="rf-link" href="${url.registrationUrl}">${msg("doRegister")}</a></span>
            </div>
        </#if>
    </#if>

</@layout.registrationLayout>
