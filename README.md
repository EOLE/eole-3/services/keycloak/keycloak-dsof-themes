# Keycloak-Dsof-Themes


Themes dans 'src/main/resources/theme'

    - 1 themes par dossier
    - le nom du thème est le nom du dossier
    - pour chaque thème, il existe plusieurs étapes du workflow keycloak (login, account, admin, welcome, ....)
    - liste des thèmes 
        - dsof-themes
    
Catalogue des thèmes  dans 'src/main/resources/META-INF'

    'keycloak-themes.json' (format utilisé par keycloak)
    
Run :

    ./mvnw package

packaging dans: ( à injecter dans eole3_keycloak/libs )

     'target/dsos-themes-xxxx-eole3.jar'


Test en local :

    bash run-local.sh
    
    fait :
    - build jar
    - créer docker image 'dsfr-themes:latest' from eole3_keycloak
    - démarre image dsof-themes (donc, avec nouveau Jar)
    - Attend démarrage
    - Intialise keycloak avec création gilles, tests
    - ouvre firefox http://localhost:8080/auth/realms/master/protocol/openid-connect/auth?client_id=security-admin-console
    tada... : le théme apparait