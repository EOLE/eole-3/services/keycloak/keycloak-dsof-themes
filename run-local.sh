#!/bin/bash 
set -e
KC_VERSION=24.0.1

echo "REDEPLOIE=$1"
# shellcheck disable=SC1091
source ./function.sh

if ! waitUrl http://localhost:8080/auth 1
then
    echo "non démarrer"
    #pkill keycloak
    run-keycloak-$KC_VERSION.sh &
    waitUrl http://localhost:8080/auth 10
    
fi
waitUrl http://localhost:8080/auth/realms/master/.well-known/openid-configuration 1

rm -rf /home/gilles/NAS1TO/keycloack/keycloak-$KC_VERSION/themes/*
cp -rf src/main/resources/theme/dsof-theme/ /opt/keycloak/keycloak-$KC_VERSION/themes/

if ! initToolsKcAdm
then
   exit 1
fi

#########################################
# login realm
#########################################
KEYCLOAK_PASSWORD="eole"
export KEYCLOAK_PASSWORD

if ! loginKeycloack
then
   exit 1
fi
# actualise le cache users !
getUsers
getUserId "admin"

#########################################
# groups
#########################################
getGroups
gidAdmin=$(getGroupId admin)

#########################################
# users
#########################################
getUsers
createUser "gilles" "eole" "$gidAdmin"
createUser "test" "test" "$gidAdmin"
createUser " test" "test" "$gidAdmin"

# actualise le cache users !
getUserId "gilles"
getUserId "test"
getUserId " test"

getClients

firefox "http://localhost:8080/auth/admin/master/console/#/realms/master"

DISPLAY=:0 chromium "http://localhost:8080/auth/realms/master/protocol/openid-connect/auth?client_id=security-admin-console"
